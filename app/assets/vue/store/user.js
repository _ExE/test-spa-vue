import UserAPI from '../api/user';

export default {
    namespaced: true,
    state: {
        isLoading: false,
        error: null,
        users: [],
    },
    getters: {
        isLoading(state) {
            return state.isLoading;
        },
        hasError(state) {
            return state.error !== null;
        },
        error(state) {
            return state.error;
        },
        hasUsers(state) {
            return state.users.length > 0;
        },
        users(state) {
            return state.users;
        },
    },
    mutations: {
        ['CREATING_USER'](state) {
            state.isLoading = true;
            state.error = null;
        },
        ['CREATING_USER_SUCCESS'](state, user) {
            state.isLoading = false;
            state.error = null;
            state.users.unshift(user);
        },
        ['CREATING_USER_ERROR'](state, error) {
            state.isLoading = false;
            state.error = error;
            state.users = [];
        },
        ['EDITING_USER'](state) {
            state.isLoading = true;
            state.error = null;
        },
        ['EDITING_USER_SUCCESS'](state, user) {
            state.isLoading = false;
            state.error = null;
            state.users.unshift(user);
        },
        ['EDITING_USER_ERROR'](state, error) {
            state.isLoading = false;
            state.error = error;
            state.users = [];
        },
        ['DELETING_USER'](state) {
            state.isLoading = true;
            state.error = null;
        },
        ['DELETING_USER_SUCCESS'](state, user) {
            state.isLoading = false;
            state.error = null;
            state.users.unshift(user);
        },
        ['DELETING_USER_ERROR'](state, error) {
            state.isLoading = false;
            state.error = error;
            state.users = [];
        },
        ['FETCHING_USERS'](state) {
            state.isLoading = true;
            state.error = null;
            state.users = [];
        },
        ['FETCHING_USERS_SUCCESS'](state, users) {
            state.isLoading = false;
            state.error = null;
            state.users = users;
        },
        ['FETCHING_USERS_ERROR'](state, error) {
            state.isLoading = false;
            state.error = error;
            state.users = [];
        },
    },
    actions: {
        createUser({commit}, login) {
            commit('CREATING_USER');
            return UserAPI.create(login)
                .then(res => commit('CREATING_USER_SUCCESS', res.data))
                .catch(err => commit('CREATING_USER_ERROR', err));
        },
        editUser({commit}, user) {
            commit('EDITING_USER');
            return UserAPI.edit(user)
                .then(res => commit('EDITING_USER_SUCCESS', res.data))
                .catch(err => commit('EDITING_USER_ERROR', err));
        },
        deleteUser({commit}, id) {
            commit('DELETING_USER');
            return UserAPI.delete(id)
                .then(res => commit('DELETING_USER_SUCCESS', res.data))
                .catch(err => commit('DELETING_USER_ERROR', err));
        },
        fetchUsers({commit}) {
            commit('FETCHING_USERS');
            return UserAPI.getAll()
                .then(res => commit('FETCHING_USERS_SUCCESS', res.data))
                .catch(err => commit('FETCHING_USERS_ERROR', err));
        },
    },
}