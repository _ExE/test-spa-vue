import axios from 'axios';

export default {
    create(login) {
        return axios.post(
            '/api/user/create',
            {
                login: login,
            }
        );
    },
    edit(user) {
        return axios.post(
            '/api/user/edit',
            {
                id: user.id,
                login: user.login,
                roles: user.roles,
            }
        );
    },
    delete(id) {
        return axios.post(
            '/api/user/delete',
            {
                id: id,
            }
        );
    },
    getAll() {
        return axios.get('/api/users');
    },
}