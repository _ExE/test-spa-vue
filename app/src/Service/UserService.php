<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Integer;
use phpDocumentor\Reflection\Types\Void_;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;

final class UserService
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param string $login
     * @param string $plainPassword
     * @return User
     */
    public function createUser(string $login, string $plainPassword): User
    {
        $user = new User();
        $user->setLogin($login);
        $user->setRoles(['ROLE_USER']);
        $password = $this->passwordEncoder->encodePassword($user, $plainPassword);
        $user->setPassword($password);
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param Request $request
     * @return User
     */
    public function editUser(Request $request): User
    {
        $id = $request->request->get('id');
        $login = $request->request->get('login');
        $roles = $request->request->get('roles');

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($id);
        $user->setLogin($login);
        $user->setRoles($roles);
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param Request $request
     */
    public function deleteUser(Request $request): void
    {
        $id = $request->request->get('id');
        $user = $this->em->getRepository(User::class)->find($id);
        $this->em->remove($user);
        $this->em->flush();
    }

    /**
     * @return object[]
     */
    public function getAll(): array
    {
        return $this->em->getRepository(User::class)->findBy([], ['id' => 'DESC']);
    }
}
