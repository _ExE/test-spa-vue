<?php

namespace App\Controller;

use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ApiUserController
 * @package App\Controller
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
final class ApiUserController extends AbstractController
{
    /** @var SerializerInterface */
    private $serializer;

    /** @var UserService */
    private $userService;

    /**
     * ApiUserController constructor.
     * @param SerializerInterface $serializer
     * @param UserService $userService
     */
    public function __construct(SerializerInterface $serializer, UserService $userService)
    {
        $this->serializer = $serializer;
        $this->userService = $userService;
    }

    /**
     * @Rest\Post("/api/user/create", name="createUser")
     * @param Request $request
     * @return JsonResponse
     * @IsGranted("ROLE_ADMIN")
     */
    public function createAction(Request $request): JsonResponse
    {
        $login = $request->request->get('login');
        $plainPassword = 'password';
        $userEntity = $this->userService->createUser($login,$plainPassword);
        $data = $this->serializer->serialize($userEntity, 'json');

        return new JsonResponse($data, 200, [], true);
    }

    /**
     * @Rest\Post("/api/user/edit", name="editUser")
     * @param Request $request
     * @return JsonResponse
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAction(Request $request): JsonResponse
    {
        $user = $this->userService->editUser($request);
        $data = $this->serializer->serialize($user, 'json');

        return new JsonResponse($data, 200, [], true);
    }

    /**
     * @Rest\Post("/api/user/delete", name="deleteUser")
     * @param Request $request
     * @return JsonResponse
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request): JsonResponse
    {
        $this->userService->deleteUser($request);

        return new JsonResponse('', 200, [], true);
    }

    /**
     * @Rest\Get("/api/users", name="getAllUsers")
     * @return JsonResponse
     * @IsGranted("ROLE_ADMIN")
     */
    public function getAllActions(): JsonResponse
    {
        $userEntities = $this->userService->getAll();
        $data = $this->serializer->serialize($userEntities, 'json');

        return new JsonResponse($data, 200, [], true);
    }
}
